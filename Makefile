# Makefile

all: blackjack minesweeper vending_machine simple_int_set

clean:
	rm blackjack minesweeper vending_machine simple_int_set

blackjack: blackjack.cc
	g++ -o blackjack blackjack.cc

minesweeper: minesweeper.h minesweeper.cc minesweeper_main.cc
	g++ -o minesweeper minesweeper.cc minesweeper_main.cc

vending_machine: vending_machine.h vending_machine.cc vending_machine_main.cc
	g++ -o vending_machine vending_machine.cc vending_machine_main.cc

simple_int_set: simple_int_set.h simple_int_set.cc simple_int_set_main.cc
	g++ -o simple_int_set simple_int_set.cc simple_int_set_main.cc

