// simple_int_set_main.cc

#include <iostream>
#include <string>
#include <vector>
#include "simple_int_set.cc"
#include "int_set_util.h"

using namespace std;

int main() {
  vector<int> int_array;
  while (true) {
    SimpleIntSet set0, set1;
    if (InputIntSet(&int_array) == false) break;
    set0.Set(&int_array[0], int_array.size());
    string op;
    cin >> op;
    if (op != "+" && op != "-" && op != "*") break;
    if (InputIntSet(&int_array) == false) break;
    set1.Set(&int_array[0], int_array.size());
    SimpleIntSet res(op == "+" ? set0.Union(set1) :
                     op == "-" ? set0.Difference(set1) : set0.Intersect(set1));
    PrintSimpleIntSet(res);
  }
  return 0;
}

