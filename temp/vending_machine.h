// vending_machine.h

#ifndef _VENDING_MACHINE_H_
#define _VENDING_MACHINE_H_

#include <map>
#include <string>

using namespace std;

struct BeverageInfo {
  //BeverageInfo(const string& n,const int& p,const int& s){name_=n; unit_price_=p; stock_=s;}
//젠장 미친. 생성자를 정상적으로 정의하려고 하면 ";"이 필요하다는 헛소리를 도대체 왜 컴파일러 이 개갞끼가 지껄여대지?
  string name;
  int unit_price;
  int stock;
};

class VendingMachine {
 public:
  VendingMachine(){}

  void AddBeverage(const string& name, int unit_price, int stock);
  int Buy(const map<string, int>& items, int money);

  typedef map<string, BeverageInfo>::const_iterator const_iterator;
  const_iterator begin() const { return beverages_.begin(); }
  const_iterator end() const { return beverages_.end(); }

 private:
  map<string, BeverageInfo> beverages_;
};


#endif  // _VENDING_MACHINE_H_
