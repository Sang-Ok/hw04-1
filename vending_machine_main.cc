// vending_machine_main.cpp

#include <iostream>
#include <string>
#include <vector>
#include "vending_machine.cc"

using namespace std;

void PrintVendignMachine(const VendingMachine& vm) {
  for (VendingMachine::const_iterator it = vm.begin(); it != vm.end(); ++it) {
    const BeverageInfo& info = it->second;
    cout << info.name << " " << info.unit_price << " " << info.stock << endl;
  }
}

int main() {
  VendingMachine vm;
  while (true) {
    string cmd;
    cin >> cmd;
    if (cmd == ":add") {
      string name;
      int unit_price, stock;
      cin >> name >> unit_price >> stock;
      vm.AddBeverage(name, unit_price, stock);
      PrintVendignMachine(vm);
    } else if (cmd == ":buy") {
      int money, num_items;
      map<string, int> items;
      cin >> money >> num_items;
      for (int i = 0; i < num_items; ++i) {
        string name;
        int count;
        cin >> name >> count;
        items[name] = count;
      }
      if ((cin.rdstate() & istream::failbit) != 0) break;
      int change = vm.Buy(items, money);
      if (change < 0) {
        if (change == -1) cout << "insufficient money" << endl;
        if (change == -2) cout << "out of stock" << endl;
        if (change == -3) cout << "unknown name" << endl;
      } else {
        cout << change << endl;
      }
    } else if (cmd == ":list") {
      PrintVendignMachine(vm);
    } else {
      break;
    }
  }
  return 0;
}

