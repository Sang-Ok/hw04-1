// int_set_util.h

#ifndef _INT_SET_UTIL_H_
#define _INT_SET_UTIL_H_

#include <stdlib.h>
#include <iostream>
#include <string>
#include <vector>

inline bool IsInt(const std::string& str) {
  bool ret = false;
  for (int i = 0; ret == false && i < str.size(); ++i) {
    ret = ('0' <= str[i] && str[i] <= '9');
  }
  return ret;
}

inline bool InputIntSet(std::vector<int>* s) {
  std::string str;
  std::cin >> str;
  if (str != "{") return false;
  s->clear();
  while (true) {
    std::cin >> str;
    if (IsInt(str)) s->push_back(atoi(str.c_str()));
    else if (str == "}") return true;
    else return false;
  }
}

inline void PrintSimpleIntSet(const SimpleIntSet& s) {
  std::cout << "{";
  for (int i = 0; i < s.size(); ++i) std::cout << " " << s.values()[i];
  std::cout << " }" << std::endl;
}

#endif  // _INT_SET_UTIL_H_
